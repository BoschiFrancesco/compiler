grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
}

@parser::members{
int stErrors=0;

private int nestingLevel = 0;
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

@lexer::members {
int lexicalErrors=0;
}
 
/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
  
prog returns [Node ast]
	: {HashMap<String,STentry> hm = new HashMap<String,STentry> ();
       symTable.add(hm);}          
	  ( e=exp 	
        {$ast = new ProgNode($e.ast);} 
      | LET d=declist IN e=exp  
        {$ast = new ProgLetInNode($d.astlist,$e.ast);}      
	  )
	  {symTable.remove(nestingLevel);}
      SEMIC EOF ; 

/**
 * Ogni elemento funzionale occupa offset doppio, poich� nelle funzioni usate come parametri la funzione non conoscerebbe l'AR della funzione parametro, quindi il chiamante deve passargli una coppia di valori rispettivamente l'indirizzo dellAR nella quale la  fuznioe � dichiarata
 * e l'indirizzo per accedere al codice della funzione. Quindi questa sar� la nmuova struttura per qualunque elemento funzionale
 */
declist	returns [ArrayList<Node> astlist]        
	: {$astlist= new ArrayList<Node>() ;
	   int offset=-2; }      
	  ( (
            VAR i=ID COLON t=hotype ASS e=exp 
            {
             /*
        	 * Viene aggiunta in symbolTable per il livello corrente se non è già presente una variabile o funzione con lo stesso nome.
        	 * Se l'operazione ha avuto successo, l'offset viene decrementato a seconda del tipo (-2 per i funzionali).
        	 */
             VarNode v = new VarNode($i.text,$t.ast,$e.ast);  
             $astlist.add(v);                                 
             HashMap<String,STentry> hm = symTable.get(nestingLevel);
             if ( hm.put($i.text,new STentry(nestingLevel,$t.ast,offset)) != null  ) {
              System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
              stErrors++; 
              }
              offset -= ((v.getSymType() instanceof ArrowTypeNode) ? 2 : 1);
                
            } 
            
      |  
            FUN i=ID COLON tt=type
              {//inserimento di ID nella symtable
              	/*
        	    * Viene aggiunta in symbolTable per il livello corrente se non � gi� presente una variabile/funzione con lo stesso nome.
        	    * Se l'operazione ha avuto successo l'offset viene decrementato di due.
        	    * Viene inoltre creata una nuova hashmap nella symbolTable per il livello successivo di nesting level.
        	    */
               FunNode f = new FunNode($i.text,$tt.ast);      
               $astlist.add(f);                              
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
               /*creo la dichiarazione di funzione ancora senza parametri pich� ancora non noti*/
               STentry entry = new STentry(nestingLevel, offset);
               if ( hm.put($i.text, entry) != null  ) {
                System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                stErrors++; 
                }
                offset-=2;
                //creare una nuova hashmap per la symTable contenente i parametri
                nestingLevel++;
                HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
                symTable.add(hmn);
                }
              LPAR 
              {	
               /*creo l'elenco dei parametri tutti discostati dell'offset prefissato: 1 nel caso di parametri non fuzionale, 2 nel caso di patametri funzionali''*/
          
              	int paroffset=0;
              	ArrayList<Node> parTypes = new ArrayList<Node>();
              }
                (fid=ID COLON fty=hotype
                  { 
                  /* Aggiungo il tipo al parametro */
                  parTypes.add($fty.ast);
                  ParNode fpar = new ParNode($fid.text,$fty.ast); /* Creo nodo ParNode */
                  f.addPar(fpar);                                 /* Attacco il nuovo nodo alla funzione */
                  /* Se il parametro è di tipo funzionale alloca doppio spazio  */
                  paroffset += ((fpar.getSymType() instanceof ArrowTypeNode) ? 2 : 1);
                  /* Aggiungo la nuova stentry del parametro alla symbol table dei parametri*/
                  if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset)) != null  ) { //aggiungo dich a hmn
                   System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
                   stErrors++; }
                  } 
                  (COMMA id=ID COLON ty=hotype
                  	/* Svolgo lo stesso procedimento per ogni altro parametro della funzione */
                    {
                    parTypes.add($ty.ast);
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
	                paroffset += ((par.getSymType() instanceof ArrowTypeNode) ? 2 : 1);
                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset)) != null  ) {
                     System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                     stErrors++; }
                    }
                  )*
                )? 
              RPAR
              {
              	/* Una volta noti i parametri, setto tali parametri alla funzione precedentemente aggiunta in symbol table che era incompleta */
	          	Node newNode = new ArrowTypeNode(parTypes,$tt.ast); 
	          	entry.addType(newNode); 
	          	f.setSymType(newNode);
	          }
              (LET d=declist IN {f.addDec($d.astlist);})? e=exp
              {f.addBody($e.ast);
               /* Rimuovo la hashmap corrente e decremento il nestingLevel siccome esco dallo scope */               
               symTable.remove(nestingLevel--);    
              }
      ) SEMIC
    )+          
	;
	
type	returns [Node ast]
  : INT  {$ast=new IntTypeNode();}
  | BOOL {$ast=new BoolTypeNode();} 
	;
	
/* Hotype ritorna un arrow nel caso in cui si tratti di un tipo funzionale, altrimenti intero/booleano ovvero i tipi base del linguaggio */
hotype  returns [Node ast]
		: t=type{$ast = $t.ast;}
        | a=arrow{$ast = $a.ast;} 
        ;
/* Arrow ritorna un ArrowTypeNode, contenente una serie di parametri ed un tipo di ritorno.
 * I parametri possono a loro volta essere degli ArrowTypeNode, dando quindi la possibilità di averne più di uno innestati
 */
arrow returns [Node ast]
	: {ArrayList<Node> parTypes = new ArrayList<Node>();}
	  LPAR (p1=hotype {parTypes.add($p1.ast);} 
	  		 (COMMA p2=hotype {parTypes.add($p2.ast);})*
	  		)? RPAR ARROW t=type {$ast = new ArrowTypeNode(parTypes, $t.ast);}
	; 

exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS x=term
 	     {$ast= new PlusNode ($ast,$x.ast);}
	     | OR x=term
	       {$ast= new OrNode ($ast,$x.ast);}
	     | MINUS x = term
	     	{$ast = new MinusNode($ast,$x.ast);} 
 	    )*
 	;
 	
term	returns [Node ast]
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     	{$ast= new TimesNode ($ast,$l.ast);}
	     | DIV l = factor
	     	{$ast= new DivNode ($ast,$l.ast);}
	     | AND l = factor
	     	{$ast= new AndNode ($ast,$l.ast);}	 
	    )*
	;
	
/* Dipendentemente dall'operatore di confronto usato (>=,<=, ==) 
 * ritorno il tipo di nodo corrispondente con i due operandi 
 che devono essere confrontati passati come parametri */
factor	returns [Node ast]
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode ($ast,$l.ast);}
	     | LE l = value
	     {$ast= new LessEqualNode ($ast,$l.ast);}
	     | GE l= value
	     {$ast = new GreaterEqualNode($ast,$l.ast);} //
	    )*
 	;	 	
 
value	returns [Node ast]
	: n=INTEGER   
	  {$ast= new IntNode(Integer.parseInt($n.text));}  
	| TRUE 
	  {$ast= new BoolNode(true);}  
	| FALSE
	  {$ast= new BoolNode(false);}  
	| LPAR e=exp RPAR
	  {$ast= $e.ast;}  
	| IF x=exp THEN CLPAR y=exp CRPAR 
		   ELSE CLPAR z=exp CRPAR 
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	
	| NOT LPAR b=exp RPAR
	  {$ast = new NotNode($b.ast);} 
	| PRINT LPAR e=exp RPAR	
	  {$ast= new PrintNode($e.ast);}
	| i=ID 
	  {//cercare la dichiarazione
           int j=nestingLevel;
           STentry entry=null; 
           while (j>=0 && entry==null)
             entry=(symTable.get(j--)).get($i.text);
           if (entry==null) {
             System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
             stErrors++; }               
	   $ast= new IdNode($i.text,entry,nestingLevel);} 
	   ( LPAR
	   	 {ArrayList<Node> arglist = new ArrayList<Node>();} 
	   	 ( a=exp {arglist.add($a.ast);} 
	   	 	(COMMA a=exp {arglist.add($a.ast);} )*
	   	 )? 
	   	 RPAR
	   	 {$ast= new CallNode($i.text,entry,arglist,nestingLevel);} 
	   )?
 	; 

  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS  	: '+' ;
MINUS   : '-' ;
TIMES   : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ; 
COMMA	: ',' ;
DOT	    : '.' ;
OR	    : '||';
AND	    : '&&';
NOT	    : '!' ;
GE	    : '>=' ;
LE	    : '<=' ;
EQ	    : '==' ;	
ASS	    : '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	    : 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;	
IN      : 'in' ;	
VAR     : 'var' ;
FUN	    : 'fun' ; 
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	  
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 

ID  	: ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;


WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;
 
ERR   	 : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 