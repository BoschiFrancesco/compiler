package lib;
import java.util.List;

import ast.*;

public class FOOLlib {
	
  public static int typeErrors = 0;	

  //valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
  public static boolean isSubtype (Node a, Node b) {
	  //se entrambe sono int o bool
	if(a.getClass().equals(b.getClass()) && (a instanceof BoolTypeNode || a instanceof IntTypeNode)) {
		return true;
	}
	//se b � bool e a int
	if((a instanceof BoolTypeNode) && (b instanceof IntTypeNode)) {
		return true;
	}
	//controllo per il subtyping tra funzioni:
	//B � sottotipo di A se hanno lo stesso numero di parametri, sono entrambi funzioni, e rispetttano la regola di covarianza tra tipi di ritorno e controvarianza tra tipi dei parametri.
	//Nella pratica, il tipo di ritorno di B deve essere sottotipo del tipo di ritorno di A mentre ciascun parametro di A deve essere sottotipo del corrispettivo parametro di B.
	if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
		ArrowTypeNode nodeA = ((ArrowTypeNode) a);
		ArrowTypeNode nodeB = ((ArrowTypeNode) b);
		List<Node> parListA = nodeA.getParList();
		List<Node> parListB = nodeB.getParList();
		if (parListA.size() == parListB.size()) {
			for (int i = 0; i < parListA.size(); i++) {
				if (!isSubtype(parListB.get(i), parListA.get(i))) {
					return false;
				}
			}
			return isSubtype(nodeA.getRet(), nodeB.getRet());
		} else {
			return false;
		}
	} else {
		return false;
	}
  }
	
  private static int labCount=0; 

  public static String freshLabel() { 
	return "label"+(labCount++);
  }  
  
  private static int funlabCount=0; 

  public static String freshFunLabel() { 
	return "function"+(funlabCount++);
  }  
  
  private static String funCode="" ; 

  public static void putCode(String c) { 
    funCode+="\n"+c; //aggiunge una linea vuota di separazione prima di funzione
  } 
  
  public static String getCode() { 
    return funCode;
  } 

  
}
