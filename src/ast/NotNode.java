package ast;

import lib.FOOLlib;
import lib.TypeException;

public class NotNode implements Node{
	
	private final Node exp;

	public 	NotNode(final Node n) {
		this.exp = n;
	}
	
	public String toPrint(final String s) {
		return s+"Not\n" + exp.toPrint(s+"  ") ;
	}

    public Node typeCheck() throws TypeException {
    	if ( !(FOOLlib.isSubtype(exp.typeCheck(), new BoolTypeNode())) ) 
    		throw new TypeException("Non boolean condition in this expression ");
    	return new BoolTypeNode();
    }
	 /*
	  *   Viene pushata sullo stack la codegen del nodo che si vuole negare e il valore "1":
	  *   se i due elementi sono equivalenti allora il termine � positivo e dunque la sua negazione � 0, quindi viene pushato 0 sullo stack.
	  *   In caso contrario, il termine � negativo e dunque la sua negazione � 0, quindi viene pushato 1 sullo stack.
	  */
	public String codeGeneration() {
		final String l1= FOOLlib.freshLabel();
		final String l2= FOOLlib.freshLabel();
	    return exp.codeGeneration()+
	    		 "push 1\n"+ 
				 "beq "+l1+"\n"+ 
				 "push 1\n"+
				 "b "+l2+"\n"+
				 l1+": \n"+
   				 "push 0\n"+
				 l2+": \n"; 
	}

}
