package ast;

import lib.FOOLlib;
import lib.TypeException;

public class OrNode implements Node{

	  private final Node left;
	  private final Node right;
	  
	  public OrNode (final Node l, final Node r) {
	   this.left=l;
	   this.right=r;
	  }
	  
	  public String toPrint(final String s) {
	   return s+"Or\n" + left.toPrint(s+"  ")  
	                      + right.toPrint(s+"  "); 
	  }
	
	  public Node typeCheck() throws TypeException {
		  if ( ! ( FOOLlib.isSubtype(left.typeCheck(), new BoolTypeNode()) &&
			         FOOLlib.isSubtype(right.typeCheck(), new BoolTypeNode()) ) ) 
				throw new TypeException("Non boolean in this expression");
			return new BoolTypeNode();
	  }
	  /*
	   * Come l'AND, effettua la codegen di uno dei due "fattori" e pusha 1 sullo stack, se i due valori sono equivalenti l'esito del costrutto OR � positivo: 
	   * pusha 1 sullo stack e salta all'etichetta finale.
	   * Se invece il primo controllo d� esito negativo, pusha la codegen del secondo nodo sullo stack e un "1", se questi due valori sono equivalenti allora l'esito del costrutto OR � positivo.
	   * In caso contrario, l'esito del costrutto OR � negativo in quanto entrambi i termini sono negativi e sullo stack viene pushato 0.
	   */
	  public String codeGeneration() {
		  final String l1= FOOLlib.freshLabel();
		  final String l2= FOOLlib.freshLabel();
		    return
                left.codeGeneration()+
                "push 1\n"+ 
 				"beq "+l2+"\n"+
 				right.codeGeneration()+
 				"push 1\n"+
 				"beq "+l2+"\n"+
 				"push 0\n"+
 				"b "+l1+"\n"+
 				l2+": \n"+
 				"push 1\n"+
 				l1+": \n";
	  }

}
