package ast;

import lib.FOOLlib;
import lib.TypeException;

public class AndNode implements Node {

	  private final Node left;
	  private final Node right;
	  
	  public AndNode (final Node left, final Node right) {
		  this.left = left;
		  this.right = right;
	  }
	  
	  public String toPrint(final String s) {
	   return s+"And\n" + left.toPrint(s+"  ")  
	                      + right.toPrint(s+"  "); 
	  }
	
	  public Node typeCheck() throws TypeException {
			Node l= left.typeCheck();  
			Node r= right.typeCheck();  
		    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) 
		    	throw new TypeException("Incompatible types in and");
		    return new BoolTypeNode();
	  }
	  /*	
	   * 	Il costrutto AND da esito positivo quando entrambi i termini sono positivi; 
	   * 	per verificare ci� si � scelto di confrontare a livello "booleano" (0,1) se entrambi i termini sono uguali ad "1".
	   * 	Viene pushato il primo elemento sullo stack assieme ad un "1", se questi due elementi sono equivalenti si verifica allo stesso modo
	   * 	l'equivalenza tra il secondo termine del costrutto AND ed un nuovo "1" pushato sullo stack. Se anche questo confronto da esito positivo,
	   * 	sullo stack viene pushato un "1" che specifica l'esito positivo della valutazione.
	   * 	In caso uno dei due confronti dia esito negativo, sullo stack viene pushato "0".
	   * 
	   */
	  public String codeGeneration() {
		  	final String l1= FOOLlib.freshLabel();
		  	final String l2= FOOLlib.freshLabel();
		  	final String l3= FOOLlib.freshLabel();
		    return left.codeGeneration()+
                 "push 1\n"+ 
   				 "beq "+l1+"\n"+ 
   				 "push 0\n"+     
   				 "b "+l2+"\n"+
   				 l1+": \n"+
                 right.codeGeneration()+
   				 "push 1\n"+
   				 "beq "+l3+"\n"+
   				 "push 0\n"+
   				 "b "+l2+"\n"+
   				 l3+": \n"+
   				 "push 1\n"+
   				 l2+": \n"; 
	  }

}
