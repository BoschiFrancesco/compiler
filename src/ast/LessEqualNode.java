package ast;

import lib.FOOLlib;
import lib.TypeException;

public class LessEqualNode implements Node {

	//left operand
	private final Node left;
	
	//right operand
	private final Node right;
	
	//Constructor
	public LessEqualNode(final Node left, final Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(final String s) {
		return s+"Less equal\n" + this.left.toPrint(s+"  ")   
           + this.right.toPrint(s+"  ") ;
	}

	@Override
	public Node typeCheck() throws TypeException {
		final Node l= this.left.typeCheck();  
		final Node r= this.right.typeCheck();  
	    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) 
	    	throw new TypeException("Incompatible types in less equal");
	    return new BoolTypeNode();
	}

	/*
	 * 	Se il primo elemento � minore o uguale del secondo, salta ad 'l1' e da esito positivo pushando 1 sullo stack.
	 * 	Se invece il primo elemento � maggiore del secondo, da esito negativo pushando zero sullo stack e saltando all'etichetta finale.
	 */
	@Override
	public String codeGeneration() {
	    final String l1= FOOLlib.freshLabel();
	    final String l2= FOOLlib.freshLabel();
		  return this.left.codeGeneration()+
				 this.right.codeGeneration()+
				 "bleq "+l1+"\n"+ 
				 "push 0\n"+    
				 "b "+l2+"\n"+
				 l1+": \n"+
				 "push 1\n"+
				 l2+": \n";	  
	}

}
