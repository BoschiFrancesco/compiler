package ast;
import lib.*;

public class IdNode implements Node {

  private String id; //Id di variabile o funzione
  private int nestingLevel; //NestingLevel dell'id
  private STentry entry; //Entry nella Symble Table della dichiarazione dell'id
  
  
  public IdNode (String i, STentry st, int nl) {
    id=i;
    nestingLevel=nl;
    entry=st;   
  }
  
  public String toPrint(String s) {
	   return s+"Id:" + id + " at nestinglevel "+ nestingLevel + "\n" + 
			   entry.toPrint(s+"  ");  
  }

  public Node typeCheck() throws TypeException {
	return entry.getType();
  }
  
  public String codeGeneration() {
	  String getAR="";
	  for (int i=0; i<nestingLevel-entry.getNestingLevel();i++)
		  getAR+="lw\n";
	  //cokme ogni elemento, se � di tipo funzionale non dovr� prendere un solo valore ma due valori: uno che fa riferimento alla dichiarazione e uno che fa riferimento al codice
	  if(entry.getType() instanceof ArrowTypeNode) {
		  return "lfp\n"+
	              getAR+ // Risalgo la catena statica degli AL per ottenere l'indirizzo dell'AR che contiene la dichiarazione dell' id
		         "push "+entry.getOffset()+"\n"+
				 "add\n"+
				 "lw\n"+
				 "lfp\n"+
				// Risalgo la catena statica degli AL per pushare l'indirizzo del codice
				  getAR+
				 "push "+(entry.getOffset()-1)+"\n"+
				 "add\n"+
				 "lw\n"; // Carico l'indirizzo della funzione sulla cima dello stack
	  }
	  return "lfp\n"+
              getAR+ // Risalgo la catena statica degli AL per ottenere l'indirizzo dell'AR che contiene la dichiarazione di id 
             "push "+entry.getOffset()+"\n"+
			 "add\n"+
             "lw\n"; // Carico l'indirizzo della funzione sulla cima dello stack
  }
}  