package ast;
import java.util.ArrayList;
import lib.*;

public class FunNode implements Node, DecNode {

  private String id;  //Id della funzione
  private Node type;  //Tipo di ritorno 
  private ArrayList<Node> parlist = new ArrayList<Node>(); //Lista dei parametri
  private ArrayList<Node> declist = new ArrayList<Node>(); //Lista delle dichiarazioni
  private Node exp; //Corpo della funzione
  private Node symType; //Tipo del nodo messo nella Symble Table
  
  public FunNode (String i, Node t) {
    id=i;
    type=t;
  }
  
  public void addDec (ArrayList<Node> d) {
    declist=d;
  }  

  public void addBody (Node b) {
	exp=b;
  }  

  public void addPar (Node p) {
    parlist.add(p);  
  }  
  
  public String toPrint(String s) {
		 String parlstr="";
		 for (Node par:parlist) 
			 parlstr+=par.toPrint(s+"  ");
		 String declstr="";
		 for (Node dec:declist) 
			 declstr+=dec.toPrint(s+"  ");
	   return s+"Fun:" + id +"\n"
			   +type.toPrint(s+"  ")
			   +parlstr
			   +declstr
               +exp.toPrint(s+"  ");
  }
  
  public Node getSymType(){
	  return symType;
  }
  
  public void setSymType(Node t) {
	  symType = t;
  }
  
  public Node typeCheck() throws TypeException {
	  for (Node dec:declist)
	    try {
	      dec.typeCheck(); } 
	    catch (TypeException e) {
	      System.out.println("Type checking error in a declaration: "+e.text); }    	
      if ( !FOOLlib.isSubtype(exp.typeCheck(),type) ) 
    	  throw new TypeException("Wrong return type for function "+id);
      return null;
  }
	     
  public String codeGeneration() {
	  //per ogni parametro/dichiarazione di tipo funzionale aggiungo un ulteriore pop inquanto occupano offset doppio
	  String declCode="", popDecl="", popParl="";
	  for (Node dec:declist) {
	      declCode+=dec.codeGeneration();
	      if(((DecNode) dec).getSymType() instanceof ArrowTypeNode) {
	    	  popDecl+="pop\n"; 
	      }
	      popDecl+="pop\n"; 
	  }
	  for (Node par:parlist) {
		  if(((DecNode) par).getSymType() instanceof ArrowTypeNode) {
			  popParl+="pop\n"; 
	      }
	      popParl+="pop\n";
	  }
	  
	  String funl=FOOLlib.freshFunLabel();
	  FOOLlib.putCode(
			    funl+":\n"+ 
			    "cfp\n"+ // Il frame pointer ora punter� alla cima dello stack		
				"lra\n"+ // Inserisce nello stack il Result address contenuto in RA, cos� da poterlo poi recuperare per far ripartire il codice dall'indirizzo corretto
	    		declCode+ // Inserisce le nuove dichiarazioni locali
	    		exp.codeGeneration()+// Genera il codice del body della funzione
	    		"stm\n"+ // Salviamo temporaneamente il valore calcolato dalla
				         // funzione prima di ripulire lo stack (pop del return value) in un registro temporaneo
	    		popDecl+ // Rimuoviamo le dichiarazioni dallo stack
	    		"sra\n"+ // Pop del ra
	    		"pop\n"+ // Pop del Access link
	    		popParl+ // Rimuoviamo i parametri dallo stack
	    		"sfp\n"+ // Setto $fp al valore del Control Link memorizzato precedentemento in quantoil controllo ritorner� al chiamante
	    		"ltm\n"+ // Inseriamo nello stack il risultato della funzione
	    		"lra\n"+ // Salviamo il valore di ra sullo stack
	    		"js\n" // Infine facciamo un salto incondizionato al result address.
	    		//Il controllo verr� quindi restuito al chiamante, l'esecuzione ripartit� da dove si era interrotta
			  );	  
	  /* Carichiamo sullo stack il valore di fp (indirizzo a questo AR)
	   e l'indirizzo della funzione (etichetta generata). */
	  return "lfp\n"+
			 "push "+ funl +"\n";
  }

}  