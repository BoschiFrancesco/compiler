package ast;

import lib.FOOLlib;
import lib.TypeException;

public class GreaterEqualNode implements Node {

	/*
	 * The two nodes to be compared
	 */
	private final Node left;
	private final Node right;

	public GreaterEqualNode(final Node left, final Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(final String indent) {
		return indent + "Greater equal\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		Node l = this.left.typeCheck();
		Node r = this.right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			throw new TypeException("Incompatible types in greater equal");
		}
		return new BoolTypeNode();
	}
	/*
	 * Stesso identica codegen del "LessEqualNode", con la differenza che viene invertito l'ordine di inserimento delle codegen dei due termini nello stack.
	 */
	  public String codeGeneration() {
		    final String l1= FOOLlib.freshLabel();
		    final String l2= FOOLlib.freshLabel();
		    return this.right.codeGeneration()+
                    this.left.codeGeneration()+
                    "bleq " + l1 +"\n" +
                    "push 0\n" +
                    "b "+ l2 +"\n" +
                    l1 + ": \n" +
                    "push 1\n" +
                    l2 + ": \n"; 
		  }
}
