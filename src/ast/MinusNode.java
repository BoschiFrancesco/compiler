package ast;

import lib.FOOLlib;
import lib.TypeException;

public class MinusNode implements Node {

	private final Node minuend;
	private final Node subtrahend;

	public MinusNode(final Node minuend, final Node subtrahend) {
		this.minuend = minuend;
		this.subtrahend = subtrahend;
	}

	@Override
	public String toPrint(final String indent) {
		return indent + "Minus\n " + this.minuend.toPrint(indent + " ") + this.subtrahend.toPrint(indent + " ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(this.minuend.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(this.subtrahend.typeCheck(), new IntTypeNode())))
			throw new TypeException("Non integers in sum");
		return new IntTypeNode();
	}

	//pusha syllo stack i due operando e l'operatore sub che effettua il pop dei due operandi e li sottrae pushando il risultato sullo stack.
	@Override
	public String codeGeneration() {
		return this.minuend.codeGeneration() + this.subtrahend.codeGeneration() + "sub\n";
	}

}
