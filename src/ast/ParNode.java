package ast;

public class ParNode implements Node, DecNode {

  private String id; // Id parametro
  private Node type; // Tipo del parametro
  
  public ParNode (String i, Node t) {
   id=i;
   type=t;
  }
  
  public String toPrint(String s) {
	   return s+"Par:" + id +"\n"
			   +type.toPrint(s+"  "); 
  }

  //non utilizzato
  public Node typeCheck() {return null;}
   
  //non utilizzato
  public String codeGeneration() {return "";}
  
  public Node getSymType(){
	  return type;
  }

}  